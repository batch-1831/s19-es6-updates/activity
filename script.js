// #3 - #4
let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

// #5 - #6
let address = [258,"Washington Ave NW", "California", 90011];
const [streetNum, city, country, code] = address;
console.log(`I live at ${streetNum} ${city}, ${country} ${code}`);

// #7 - #8
let animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    weight: 1075,
    height: "20 ft 3 in"
}

const {name, type, weight, height} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${height}.`);

// #9-#11
let numArray = [1, 2, 3, 4, 5];
numArray.forEach(number => console.log(number));

let reduceNumber = numArray.reduce((x,y) => x + y);
console.log(reduceNumber);

// #12 - #13

class Dog{
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog("Frankie", 5, "Miniature Dachsund");
console.log(myDog);